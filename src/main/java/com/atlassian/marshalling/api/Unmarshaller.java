package com.atlassian.marshalling.api;

import com.atlassian.annotations.PublicSpi;

/**
 * Represents the ability to recreate an object, of type <tt>T</tt>, from an array of {@code byte}s.
 * <p>Notes:</p>
 * <ul>
 * <li>
 * Data versioning is the responsibility of the implementation.
 * </li>
 * <li>
 * The instances returned by {@link #unmarshallFrom(byte[])} for equal input byte arrays should match when
 * compared using the {@link Object#equals(Object)} method, but it's not a hard requirement.
 * </li>
 * <li>
 * The implementation must be multi-thread safe.
 * </li>
 * </ul>
 *
 * @param <T> the type that can be converted
 * @since 1.0
 */
@FunctionalInterface
@PublicSpi
public interface Unmarshaller<T> {
    /**
     * Responsible for converting an array of bytes into an object.
     *
     * @param raw the array of bytes to be converted.
     * @return the object represented by <tt>raw</tt>
     * @throws MarshallingException if unable to perform the conversion. Implementors should not wrap all exceptions,
     *                              like {@link NullPointerException}, with this exception. It should be used to report
     *                              specific errors related to doing the unmarshalling, such as a checked exception
     *                              like {@link java.io.IOException}.
     */
    T unmarshallFrom(byte[] raw) throws MarshallingException;
}
